console.log("Hello World!")



/* Quiz

1. What is the blueprint where objects are created from?

answer: Class

2. What is the naming convention applied to classes?

answer: Class names should be nouns, in mixed case with the first letter of each internal word capitalized.

3. What keyword do we use to create objects from a class?

answer: new

4. What is the technical term for creating an object from a class?

answer: Instantiation

5. What class method dictates HOW objects will be created from that class?

answer: constructor

*/

/* Function Coding 
1. Define a grades property in our Student class that will accept an array of 4 numbers ranging from 0 to 100 to be its value. If the argument used does not satisfy all the conditions given, this property will be set to undefined instead.

2. Instantiate all four students from the previous session from our Student class. Use the same values as before for their names, emails, and grades.

*/

class Student {

	constructor(name, email, grades){
		this.name = name;
		this.email = email;
		this.gradeAve = undefined;
		this.passed = undefined;
		this.passedWithHonors = undefined;

		if(grades.length === 4){
			if(grades.every(grade => grade >= 0 && grade <= 100))
		{
			this.grades = grades;
		}else {
			this.grades = undefined;
		}
}else {
	this.grades = undefined
}
// ===========================
		
		if(this.computeAve() >= 85){
			this.passed = true
		}else{
			this.passed = false
		}

			
// ========================
		
		if (this.willPass()) {
	    	if ((this.computeAve()) >= 90) {
	    		this.passedWithHonors = true
	    	} else {
	    		this.passedWithHonors = false;
	    	}
	    } else {
	    	this.passedWithHonors = undefined;
	    }


// =======================

}
	login(){
		console.log(`${this.email} has logged in`);
		return this;
	}
	logout(){
		console.log(`${this.email} has logged out`);
		return this;
	}
	listGrades(){
		console.log(`${this.name}'s quarterly grade averages are: ${this.grades}`);
		return this;
	}
	computeAve(){
	    let sum = 0;
	    this.grades.forEach(grade => sum = sum + grade);
	    // update the gradeAve property
	    this.gradeAve = sum/4;
	    // returns the object
	    return this;
	}
	willPass() {
	    this.passed = this.computeAve().gradeAve >= 85 ? true : false;
	    return this;
	}
	willPassWithHonors() {
	    if (this.passed) {
	        if (this.gradeAve >= 90) {
	            this.passedWithHonors = true;
	        } else {
	            this.passedWithHonors = false;
	        }
	    } else {
	        this.passedWithHonors = false;
	    }
	    return this;
	}
}



let studentOne = new Student("John", "john@mail.com", [89, 84, 78, 88]);
let studentTwo = new Student("Joe", "joe@mail.com", [78, 82, 79, 85]);
let studentThree = new Student("Jane", "jane@mail.com", [87, 89, 91, 93]);
let studentFour = new Student("Jessie", "jessie@mail.com", [91, 89, 92, 93]);



/*========================
Activity 2

Quiz:

1. Should class methods be included in the class constructor?
Answer: No

2. Can class methods be separated by commas?
Answer: No

3. Can we update an object’s properties via dot notation?
Answer: Yes

4. What do you call the methods used to regulate access to an object’s properties?
Answer: getters and setters

5. What does a method need to return in order for it to be chainable?
Answer: this


=====
Function coding:
Modify the Student class to allow the willPass() and willPassWithHonors() methods to be chainable.
Sample output:

*/